import java.util.Scanner

class Main {
	public static void main(String args[]){
		Scanner sc = new Scanner(System.In);
		String name = sc.nextLine();
		greet(name);
	}

	public void greet(String name){
		System.out.println("Hello " ++ name);
	}

}